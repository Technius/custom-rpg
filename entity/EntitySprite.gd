extends Sprite

onready var _player_anim = get_node("AnimationPlayer")

var previous_vel = Vector2(0, 1) # down

func play_walk_animation(velocity):
	if previous_vel == velocity:
		return

	var dir = _dir_from_velocity(velocity)

	if velocity == Vector2(0, 0):
		_player_anim.set_current_animation("stand_" + _dir_from_velocity(previous_vel))
		if _player_anim.is_playing():
			_player_anim.stop(false) # reset by default doesn't work and prevents further reset
			_player_anim.seek(0, true) # manual reset
	else:
		_player_anim.set_current_animation("walk_" + dir)
		if not _player_anim.is_playing():
			_player_anim.play()

	previous_vel = velocity

func _dir_from_velocity(v):
	if v == Vector2(0, 0):
		return ""
	elif v.x > 0:
		return "right"
	elif v.x < 0:
		return "left"
	elif v.y > 0:
		return "down"
	elif v.y < 0:
		return "up"