extends Node2D

onready var play_btn = get_node("PlayButton")

func _ready():
	self.play_btn.connect("button_down", self, "_on_play_btn_pressed")

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_play_btn_pressed():
	print("Play button pressed, changing to overworld")
	var flag = self.get_tree().change_scene("res://overworld/Overworld.tscn")