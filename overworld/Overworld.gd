extends Node

onready var _player = get_node("World/Player")
onready var _player_sprites = get_node("World/Player/Sprites")
var player_velocity = Vector2(0, 0)
var player_speed = 250

func _ready():
	self.set_process_unhandled_key_input(true)
	self.set_physics_process(true)

func _unhandled_key_input(event):
	_recalculate_player_velocity()
	
func _physics_process(delta):
	var vel = player_velocity.normalized() * player_speed * delta
	var rem = _player.move_and_slide(vel)
	_player.position += rem

func _recalculate_player_velocity():
	var left  = -1 if Input.is_action_pressed("move_left")  else 0
	var right =  1 if Input.is_action_pressed("move_right") else 0
	var up    = -1 if Input.is_action_pressed("move_up")    else 0
	var down  =  1 if Input.is_action_pressed("move_down")  else 0
	
	player_velocity.x = left + right
	player_velocity.y = up + down
	
	for anim in _player_sprites.get_children():
		anim.play_walk_animation(player_velocity)